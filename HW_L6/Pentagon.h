#pragma once
#include "shape.h"
#include "mathUtils.h"
#include <iostream>

class Pentagon : public Shape 
{
	double _side;
	std::string _name;
	std::string _color;
public:
	Pentagon(std::string nam , std::string col ,  double side);
	~Pentagon();

	void setSide(double side);
	double getSide();
	double CalArea();
	double CalCircumference();
	void draw();
};

