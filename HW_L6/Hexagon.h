#pragma once
#include "shape.h"
#include "mathUtils.h"

class Hexagon : public Shape
{
	double _side;
	std::string _name;
	std::string _color;
public:
	Hexagon(std::string nam, std::string col, double side);
	~Hexagon();

	void setSide(double side);
	double getSide();
	double CalArea();
	double CalCircumference();
	void draw();
};
