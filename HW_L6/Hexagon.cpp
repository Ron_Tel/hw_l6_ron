#include "Hexagon.h"

Hexagon::Hexagon(std::string nam, std::string col, double side) : Shape(nam, col)
{
	_name = nam;
	_color = col;
	_side = side;
}

Hexagon :: ~Hexagon()
{
	//nothing
}

void Hexagon::setSide(double side)
{
	_side = side;
}
double Hexagon::getSide()
{
	return (_side);
}

double Hexagon::CalArea()
{
	MathUtils math = MathUtils();
	return (math.calHexagonArea(_side));
}
double Hexagon::CalCircumference()
{
	return (_side * 6);
}
void Hexagon::draw()
{
	std::cout << "name is " << getName() << " color is " << getColor() << " area is " << this->CalArea() << " circumference is " << this->CalCircumference();
}