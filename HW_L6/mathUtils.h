#pragma once
#include <math.h>

#define PI 3.14

class MathUtils 
{
public:
	static double calPentagonArea (double side) 
	{
		return ((1.25 * side) * (cos(PI / 5) / sin (PI / 5)));
	}

	static double calHexagonArea(double side)
	{
		return ((1.5 * side) * (cos(PI / 6) / sin(PI / 6)));
	}
};
